;;; polybar.el --- Functions to interact with polybar -*- lexical-binding: t; -*-
;;
;; Copyright (C) 2022 Jake Shilling
;;
;; Author: Jake Shilling <shilling.jake@gmail.com>
;; Maintainer: Jake Shilling <shilling.jake@gmail.com>
;; Created: April 19, 2022
;; Modified: April 19, 2022
;; Version: 0.0.1
;; Homepage: https://gitlab.com/e4303/packages/polybar.el
;; Package-Requires: ((emacs "25.1") (a "1.0.0") (dash "2.19.1") (monitors "0.0.1"))
;;
;; This file is not part of GNU Emacs.
;;
;;; Commentary:
;;
;;  Functions to interact with polybar
;;
;;; Code:

(require 'map)
(require 'a)
(require 'dash)
(require 'monitors)
(require 'server)

(defvar polybar--process-alist nil
  "Holds the current polybar process if any.")

(defvar polybar--all-monitors
  (monitors-get-connected))

(defvar polybar--all-monitor-names
  (-map #'monitors-monitor-name polybar--all-monitors))

(defvar polybar--primary-monitor
  (when-let ((primary  (-first #'monitors-monitor-primary polybar--all-monitors)))
    (monitors-monitor-name primary)))

(defun polybar--monitor->key (monitor)
  "Return the argument MONITOR as a keyword.

If MONITOR is a string, then a keyword is formed by prefixing a
':'. If MONITOR is already a keyword, it is simply returned. In
any other case, an error is singaled."
  (cond ((stringp monitor) (intern (concat ":" monitor)))
        ((keywordp monitor) monitor)
        (t (error "Cannot form a polybar monitor key from %s" monitor))))

(defun polybar--key->monitor (key)
  "Undo the transformation done by `polybar--monitor->key'.

KEY should be a keyword and is turned into a string by taking the
substring of its symbol name without the ':' prefix."
  (if (keywordp key)
      (substring (symbol-name key) 1)
    (signal 'wrong-type-argument '(keywordp key))))

(defun polybar--get (monitor)
  "Get the polybar process associated with MONITOR.

Use `polybar--monitor->key' to form a key from MONITOR, then use
that result to lookup the corresponding value in
`polybar--process-alist'."
  (let ((key (polybar--monitor->key monitor)))
    (a-get polybar--process-alist key)))

(defun polybar--put (monitor buffer)
  "Save the association between MONITOR and BUFFER.

MONITOR should be the name of the monitor on which polybar is
running and BUFFER is the corresponding process buffer. These
values are saved into `polybar--process-alist' using
`polybar--monitor->key' to get the key from MONITOR."
  (let ((key (polybar--monitor->key monitor)))
    (setq polybar--process-alist (a-assoc polybar--process-alist
                                          key buffer))))

(defun polybar--delete (monitor)
  "Remove the association between MONITOR and a linked polybar process.

MONITOR is converted into a key by `polybar--monitor->key' and
then any associated value is removed from
`polybar--process-alist'."
  (let ((key (polybar--monitor->key monitor)))
    (setq polybar--process-alist (a-dissoc polybar--process-alist
                                           key))))

(defun polybar--buffers ()
  "Return all polybar process buffers."
  (a-vals polybar--process-alist))

(defun polybar--keys ()
  "Return all keys associated with polybar buffers."
  (a-keys polybar--process-alist))

(defun polybar--monitors ()
  "Return all monitor names which have polybar instances."
  (-map #'polybar--key->monitor (polybar--keys)))

(defun polybar--kill (monitor)
  "Kill polybar process associated with MONITOR."
  (when-let ((buffer (polybar--get monitor)))
    (let ((kill-buffer-query-functions nil))
      (kill-buffer buffer))
    (polybar--delete monitor)))

(defun polybar--start-polybar (monitor)
  "Start polybar on MONITOR."
  (let ((buffer (generate-new-buffer "*polybar*")))
    (with-current-buffer buffer
      (make-local-variable 'process-environment)
      (polybar--kill monitor)
      (add-to-list 'process-environment (concat "MONITOR=" monitor))
      (start-process "polybar" buffer "polybar" "panel")
      (polybar--put monitor buffer))))

;;;###autoload
(defun polybar-kill ()
  "Kill a polybar instance."
  (interactive)
  (let ((monitor (completing-read "Kill polybar on monitor:"
                                  (polybar--monitors))))
    (polybar--kill monitor)))

;;;###autoload
(defun polybar-kill-all ()
  "Kill all polybar processes."
  (interactive)
  (let ((kill-buffer-query-functions nil))
    (-each #'kill-buffer (polybar--buffers)))
  (setq polybar--process-alist nil))

;;;###autoload
(defun polybar-start ()
  "Start a polybar process."
  (interactive)
  (let ((monitor (if (> (seq-length polybar--all-monitor-names) 1)
                     (completing-read "Start polybar on monitor:"
                                      polybar--all-monitor-names)
                   (or polybar--primary-monitor
                       (car polybar--all-monitor-names)
                       (user-error "Cannot find any monitors")))))
    (polybar--start-polybar monitor)))

;;;###autoload
(defun polybar-start-primary ()
  "Start a polybar process on primary monitor."
  (interactive)
  (polybar--start-polybar (or polybar--primary-monitor
                                (car polybar--all-monitor-names)
                                (user-error "Cannot find any monitors"))))

;;;###autoload
(defun polybar-start-all ()
  "Start polybar on all known monitors."
  (interactive)
  (-each polybar--all-monitor-names #'polybar--start-polybar))

;;;###autoload
(defun polybar-send-msg (module-name hook-index)
  "Send a message to polybar.

Calls `polybar-msg' as a shell command sending MODULE-NAME and
HOOK-INDEX as arguments."
  (interactive)
  (unless (server-running-p)
    (server-start))
  (start-process-shell-command "polybar-msg" nil
                               (format "polybar-msg hook %s %s"
                                       module-name
                                       hook-index)))

(provide 'polybar)
;;; polybar.el ends here
